package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
)

func initConfiguration() ServiceConfiguration {
	var srvConfig ServiceConfiguration

	// Defaults values - start
	srvConfig.ActiveIPRequire = 1
	// Defaults values - end

	envX := os.Getenv("X")
	if envX != "" {
		X, err := strconv.ParseUint(envX, 10, 64)
		if err != nil {
			X = 1
			log.Print("Value of X (env var) is not valid, default value is use (X=1)")
		}
		srvConfig.ActiveIPRequire = X
	} else {
		srvConfig.ActiveIPRequire = 1
	}
	return srvConfig
}

func apiCompute(srvConfig ServiceConfiguration, config []IpConfig) []string {
	resultSet := make([]string, 0)

	if len(config) <= 0 {
		return resultSet
	}

	sort.Slice(config, func(i, j int) bool {
		return config[i].Hostname < config[j].Hostname
	})

	startHostname := config[0].Hostname
	iHostname := uint64(0)
	for _, row := range config {
		if startHostname != row.Hostname {
			if iHostname <= srvConfig.ActiveIPRequire {
				resultSet = append(resultSet, startHostname)
			}
			startHostname = row.Hostname
			iHostname = 0
		}
		if iHostname > srvConfig.ActiveIPRequire {
			continue
		} else if row.Active {
			iHostname += 1
		}
	}
	if iHostname <= srvConfig.ActiveIPRequire {
		resultSet = append(resultSet, startHostname)
	}

	return resultSet
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var newData []IpConfig

	if r.Body == nil {
		http.Error(w, "Error for read body inside the request", http.StatusInternalServerError)
		return
	}
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error for read body inside the request", http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(reqBody, &newData)
	if err != nil {
		http.Error(w, "JSON send inside body is invalid", http.StatusBadRequest)
		return
	}

	resultSet := apiCompute(s.SrvConfig, newData)

	jsonStr, err := json.Marshal(resultSet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonStr)
}

func main() {
	srvConfig := initConfiguration()
	s := &Server{SrvConfig: srvConfig}
	http.Handle("/", s)
	log.Print("Server going up on port: 80")
	log.Fatal(http.ListenAndServe(":80", nil))
}
