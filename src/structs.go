package main

// Server: REST API
type Server struct {
	SrvConfig ServiceConfiguration
}

// Configuration: Service configuration
type ServiceConfiguration struct {
	ActiveIPRequire uint64
}

// IpConfig: Domain Model
type IpConfig struct {
	Ip       string
	Hostname string
	Active   bool
}
