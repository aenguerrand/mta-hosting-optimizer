package main

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
)

func TestApiCompute(t *testing.T) {
	srvConfig := ServiceConfiguration{ActiveIPRequire: 1}
	ipConfig := make([]IpConfig, 6)
	ipConfig[0].Hostname = "mta-prod-1"
	ipConfig[0].Ip = "127.0.0.1"
	ipConfig[0].Active = true
	ipConfig[1].Hostname = "mta-prod-1"
	ipConfig[1].Ip = "127.0.0.2"
	ipConfig[1].Active = false
	ipConfig[2].Hostname = "mta-prod-2"
	ipConfig[2].Ip = "127.0.0.3"
	ipConfig[2].Active = true
	ipConfig[3].Hostname = "mta-prod-2"
	ipConfig[3].Ip = "127.0.0.4"
	ipConfig[3].Active = true
	ipConfig[4].Hostname = "mta-prod-2"
	ipConfig[4].Ip = "127.0.0.5"
	ipConfig[4].Active = false
	ipConfig[5].Hostname = "mta-prod-3"
	ipConfig[5].Ip = "127.0.0.6"
	ipConfig[5].Active = false

	t.Run("Api Compute subject", func(t *testing.T) {
		var wantSet []string
		wantSet = append(wantSet, "mta-prod-1")
		wantSet = append(wantSet, "mta-prod-3")
		resultSet := apiCompute(srvConfig, ipConfig)
		if !reflect.DeepEqual(resultSet, wantSet) {
			t.Errorf("got %s, want %s", resultSet, wantSet)
		}
	})

	t.Run("Api Compute empty", func(t *testing.T) {
		var wantSet []string
		wantSet = append(wantSet, "")
		ipConfig2 := make([]IpConfig, 0)
		resultSet := apiCompute(srvConfig, ipConfig2)
		if len(resultSet) > 0 {
			t.Errorf("got %s, want %s", resultSet, wantSet)
		}
	})
}

func TestServer_ServeHTTP_OK(t *testing.T) {
	var jsonStr = []byte(`[
  {"ip":"172.121.117.56","hostname":"nih.gov","active":true},
  {"ip":"127.15.57.125","hostname":"nih.gov","active":true},
  {"ip":"172.63.149.236","hostname":"nih.gov","active":true},
  {"ip":"171.35.219.32","hostname":"rakuten.co.jp","active":false},
  {"ip":"34.35.166.143","hostname":"reuters.com","active":false},
  {"ip":"233.215.35.193","hostname":"usda.gov","active":false},
  {"ip":"116.72.84.235","hostname":"whitehouse.gov","active":true},
  {"ip":"144.236.34.163","hostname":"twhitehouse.gov","active":true},
  {"ip":"80.182.201.206","hostname":"squidoo.com","active":true},
  {"ip":"229.217.139.170","hostname":"bravesites.com","active":true}
]`)
	s := &Server{SrvConfig: ServiceConfiguration{ActiveIPRequire: 2}}

	req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonStr))
	rr := httptest.NewRecorder()
	s.ServeHTTP(rr, req)
	if err != nil {
		t.Fatal(err)
	}
	if http.StatusOK != rr.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, rr.Code)
	}

	if body := rr.Body.String(); body != "[\"bravesites.com\",\"rakuten.co.jp\",\"reuters.com\",\"squidoo.com\",\"twhitehouse.gov\",\"usda.gov\",\"whitehouse.gov\"]" {
		t.Errorf("Expected an empty array. Got %s", body)
	}
}

func TestServer_ServeHTTP_Bad_JSON(t *testing.T) {
	var jsonStr = []byte(`[
  {"ip":"172.121.117.56","hostname":"nih.gov","a
]`)
	s := &Server{SrvConfig: ServiceConfiguration{ActiveIPRequire: 2}}

	req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonStr))
	rr := httptest.NewRecorder()
	s.ServeHTTP(rr, req)
	if err != nil {
		t.Fatal(err)
	}
	if http.StatusBadRequest != rr.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusBadRequest, rr.Code)
	}
}

func TestServer_ServeHTTP_Body_Nil(t *testing.T) {
	s := &Server{SrvConfig: ServiceConfiguration{ActiveIPRequire: 2}}

	req, err := http.NewRequest("POST", "/", nil)
	rr := httptest.NewRecorder()
	s.ServeHTTP(rr, req)
	if err != nil {
		t.Fatal(err)
	}
	if http.StatusInternalServerError != rr.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusInternalServerError, rr.Code)
	}
}

func TestInitConfiguration(t *testing.T) {
	var tests = []struct {
		envX string
		want uint64
	}{
		{"0", 0},
		{"1", 1},
		{"12", 12},
		{"-1", 1},
		{"", 1},
		{" ", 1},
		{"a", 1},
	}
	for _, tt := range tests {
		testname := fmt.Sprintf("%s", tt.envX)
		t.Run(testname, func(t *testing.T) {
			os.Setenv("X", tt.envX)
			srvConfig := initConfiguration()
			if srvConfig.ActiveIPRequire != tt.want {
				t.Errorf("got %d, want %d", srvConfig.ActiveIPRequire, tt.want)
			}
		})
	}
}

func TestMain(m *testing.M) {
	exitVal := m.Run()
	os.Exit(exitVal)
}
