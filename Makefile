.PHONY: build clean

build:
	go build -ldflags="-s -w" -o bin/mta-hosting-optimizer src/*.go

clean:
	rm -rf ./bin
