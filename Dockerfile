FROM golang:1.13-alpine3.11 as build

WORKDIR /go/src/app
COPY . .
RUN go build -ldflags="-s -w" -o bin/mta-hosting-optimizer src/*.go

FROM alpine:3.11

COPY --from=build /go/src/app/bin/mta-hosting-optimizer /usr/local/bin/mta-hosting-optimizer
EXPOSE 80
ENTRYPOINT ["/usr/local/bin/mta-hosting-optimizer"]