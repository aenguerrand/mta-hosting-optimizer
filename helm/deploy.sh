result=$(eval helm install --set image.tag=$CI_COMMIT_REF_SLUG \
 --set gitlab.CI_PROJECT_PATH_SLUG=$CI_PROJECT_PATH_SLUG \
 --set gitlab.CI_ENVIRONMENT_SLUG=$CI_ENVIRONMENT_SLUG \
 --set gitlab.deploy=true \
 $CI_COMMIT_REF_SLUG ./helm/mta-hosting-optimizer/)
if [ $? -ne "0" ]; then
   helm upgrade --set image.tag=$CI_COMMIT_REF_SLUG \
   --set gitlab.CI_PROJECT_PATH_SLUG=$CI_PROJECT_PATH_SLUG \
   --set gitlab.CI_ENVIRONMENT_SLUG=$CI_ENVIRONMENT_SLUG \
   --set gitlab.deploy=true \
   $CI_COMMIT_REF_SLUG ./helm/mta-hosting-optimizer/
fi