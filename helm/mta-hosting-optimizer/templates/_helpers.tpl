{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "mta-hosting-optimizer.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "mta-hosting-optimizer.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "mta-hosting-optimizer.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "mta-hosting-optimizer.labels" -}}
helm.sh/chart: {{ include "mta-hosting-optimizer.chart" . }}
{{ include "mta-hosting-optimizer.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- if .Values.gitlab.deploy }}
app.gitlab.com/app: {{ .Values.gitlab.CI_PROJECT_PATH_SLUG }}
app.gitlab.com/env: {{ .Values.gitlab.CI_ENVIRONMENT_SLUG }}
{{- end -}}
{{- end -}}

{{/*
Gitlab deploy
*/}}
{{- define "mta-hosting-optimizer.gitlabDeploy" -}}
{{- if .Values.gitlab.deploy }}
app.gitlab.com/app: {{ .Values.gitlab.CI_PROJECT_PATH_SLUG }}
app.gitlab.com/env: {{ .Values.gitlab.CI_ENVIRONMENT_SLUG }}
{{- end -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "mta-hosting-optimizer.selectorLabels" -}}
app.kubernetes.io/name: {{ include "mta-hosting-optimizer.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}