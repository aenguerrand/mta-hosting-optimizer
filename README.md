# mta-hosting-optimizer

## Requirements
*  GoLang 1.13

## Build
`make build`

## Run
`./bin/mta-hosting-optimizer`

## Run units test
`go test -cover -v ./src`

## Deploy inside a Kubernetes
`helm install --set image.tag=dev my_release ./helm/mta-hosting-optimizer/`

Inside a local Kubernetes, you can access to the service with this URL: __http://localhost__

## Metrics examples (with a deployement inside a Kubernetes link to Gitlab)
![](https://i.imgur.com/9tIy4m9.png)
![](https://i.imgur.com/OV9pbII.png)
